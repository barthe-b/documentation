---
title: R2Devops FAQ
description: Find here all the questions and their answers about R2Devops
---

# Welcome to the FAQ!

Here you'll find the answer to your questions! If the question you are looking for isn't on the FAQ, you can contact our support or open a ticket to add it 👇🏻

<a alt="Use the hub" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=faq" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        On Discord 💬
    </button>
</a>

<a alt="Use the hub" href="https://tally.so/r/w5Edvw" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        Open a ticket 🎟
    </button>
</a>

## R2Devops platform

### Organizations & private resources

??? info "How can I add a new organization in R2Devops?"

     We get your organizations directly from GitLab, so all you have to do is create a new organization in GitLab!

     To add a new organization in GitLab, follow [this tutorial](https://docs.gitlab.com/ee/topics/set_up_organization.html)!


??? info "What is a private catalog?"

     A private catalog is a personal space in R2Devops where you can securely store all your CI/CD resources.


??? info "Who can access my private catalog?"

     Only you, and your organization's members can access your private catalog!





### Pipeline generator

??? info "How do you generate automatically a pipeline?"

     We analyze the DNA of your project: its technologies, languages, and requirements.

     Then we look in your [hub](https://r2devops.io/_/hub) of open source CI/CD resources and select the ones that fit your project, and add them in your pipeline! ✌🏻


??? info "Can I generate a pipeline for a private project?"

     Yes, you just need to have an account in R2Devops to generate a pipeline for a private project.


??? info "How many pipelines can I generate for a project?"

     There is no limit number! You can generate as many pipelines as you want for your project.

     The limit for is license is related to the number of project you can use pipeline generator on:

     * 5 projects for the core license
     * 30 projects for the basic license

### Pipeline update

??? info "How the Pipeline Updates works?"

     The feature is activated as soon as you generated a pipeline for one of your project. We will check the pipeline you have generated, and see if any updates are available for the jobs in it.

     For now you'll only be notify on the platform: the `Generate my pipeline` button of the project you can update will turn orange! Email notification will be available soon.

??? info "How can I update a pipeline?"

    You just need to generate a new pipeline! THe updates we found will be automatically applied to your project.


??? info "Can I unsubscribe from the feature?"

     For now the feature is always activated onec you've generated a pipeline on a project. If you don't want to received the email anymore, you can unsubscribe from the notification list. You'll find the link to do so at the end of the emails.

??? info "I have unsubscribe from the mailing list but I want in again. What can I do?"

     Send on a message on [Discord](https://discord.r2devops.io) and we'll find a solution together!

### Access token

??? info "What are access token used for?"

    Personal access tokens allows you to use R2Devops features that interact with your repositories on external platforms (GitLab, GitHub, ...).
    Example: manage your projects pipelines directly in your R2Devops dashboard.

??? info "🦊 How to create a GitLab personal access token ?"

    In GitLab, go to the [Access Tokens](https://gitlab.com/-/profile/personal_access_tokens) page (`User settings > Access Token`).
    In the first section (named `Personal Access Token`), create a new personal access token with following parameters:

    - Token name: `r2devops.io` (you can choose any name you want)
    - Expiration date: this is not mandatory. If you set an expiration date, you will have to re-create a token and add it in R2Devops at each expiration.
    - Select scopes: select only `api`

    Once it has been created, copy the Token and add it in R2Devops. Additional information in [GitLab documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

??? info "😺 How to create a GitHub personal access token ?"

    In GitHub, go to the [Personal Access Tokens](https://github.com/settings/tokens) page (`Settings > Developer settings > Personal access tokens`).
    Click on the button `Generate new token`

    - Note: `r2devops.io` (you can choose any note you want)
    - Expiration: if you set an expiration date, you will have to re-create a token and add it in R2Devops at each expiration.
    - Select scopes: `repo` and `workflow`


??? info "How to add a perosnal access token in R2Devops?"

    Log in R2Devops, and go to your [account](https://r2devops.io/u/dashboard/profile#account) page inside your settings dashboard. Then, click on the `Add a token` button, and paste your token in the form. That's it!

### Other

??? info "What will be the next features developed in R2Devops?"

     You can take a look to the [roadmap](https://r2devops.io/roadmap){target=_blank} to see and vote for the next feature we will develop!

??? info "Will R2Devops always be free?"

     The library of open source jobs (the Hub) will always be available without license, and will stay free!

### R2Devops Hub

#### Use a job

??? info "What are R2Devops' standards of quality and security for official jobs?"

    When jobs have the label "Official", it means they respect standards of quality and safety defined by R2Devops' team.
    Regarding the quality, we ensure:

    - Fixed tag for docker image and any external tool used inside the job, so they don't break the jobs if they change.
    - Resources with license compatible with the job license, so anyone can use it.
    - Artifacts and logs production, in order to facilitate the comprehension of the job's results.
    - Simple customization of the jobs, thanks to variables.
    - The structure of the job is properly defined, in order to build a clear documentation.

    Regarding the security, take a look at our CI pipeline:

    ![R2Devops' CI pipeline](../images/CI_pipeline.png)

??? info "What is a plug and play job?"

    We described as Plug and Play the jobs that don't need configuration in order to work in your pipeline.

    It means you can add the include link of the job in your pipeline and directly run it. And it will work ✨


??? info "How do I include a job in my pipeline?"

    In order to include a job in your pipeline, you need to add the include link in your `.gitlab-ci.yml` file.

    But first, you need to precise the stage in which your job is supposed to work!

??? info "How do I delete my job?"

    In order to delete a job, you need to access the job's modification page from your contributing dashboard.
    In the bottom of the page, there is a red button to delete your job! You just have to click on it, and follow the steps.

    **Be careful: other people than you might be using your job. If you delete it, their pipelines will broke**

??? info "Why should I use an official job?"

     Official jobs are checked by R2Devops' team and respect certain standard regarding security and quality. Plus, you can be sure those jobs won't be deleted from the hub, and that your pipeline won't broke due to a missing content.

#### Add a job

??? info "How can I add a job to the hub ?"

    They are 2 ways to add a job into R2Devops. You can:

    🔥 [Link your job](/link-a-job)

    ❤️ [Contribute on the official repository](/contribute)

??? info "Why my contribution wasn't validated yet?"

      Adding your job into the official R2Devops repository will require more time than linking your job.

      Why?

      Because our team personally review your job and ensure it fits our safety and quality requirements!
      Once the first review is done, they'll let you know your work was perfect, and your job is added or if some adjustments are required.

#### Private job


??? info "What is a private job?"

    A private job is a job set up by the owner. The job is link to the GitLab organization from the repository the job came from. What makes it private is its visibility: only the owner and the people who are member of the job GitLab organization can use it.


??? info "Who can access a private job?"

    Only the owner and his/her teammates working on the project can see the job in R2Devops and use it.

    It won't be displayed on our platform for other users. The owner (or his/her teammates) must have an account on R2Devops and be connected to access it.

    But the user could share this link with anyone who need to include it in their `.gitlab-ci.yml` file.

??? info "How the private token work?"

    When you set your job visibility to private the link to include in the `.gitlab-ci.yml` file will contain a private token linked to the owner.

    Example of a private job link :
    ```
    https://r2devops.io/r/<owner_name>/<job_name>/<job_version>.yaml?file=script&token=r2-42e2g4975r.yml
    ```

    On a private job the link won't work without the token part: `token=r2-42e2g4975r`

??? info "What happens if I make my job private?"

    If you set your job to private, the job won't be accessible on our platform for others users and the link included in their .`gitlab-ci.yml` file won't work anymore.

    The owner will have to use the new link with the owner token

??? info "How can I change my job visibility?"

    They are several ways to declare your job as private.

    **At the creation:** When you import your job, in the [link a job](/link-a-job) page, you will have the possibility to click on a checkbox before the job importation to set it private.

    **After the creation :** On the job page or in your job dashboard, you can access the `Modify my job` page and declare your job as private.

!!! warning
    **_If you make your job private you will have to confirm this action by entering your job name again, remember that users who currently use it won't be able to use it anymore without your personal link_**

#### Delete a job

??? info "How do I delete my job?"

     If you want to delete your job, you need to go in your *Jobs' dashboard*, then click on *Modify my job*. At the bottom of the modification page, you'll see a button *Delete*. Click on it and follow the process. Your job will be delete once it's completed!

??? info "What happens if I delete my job?"

     If you delete your job from R2Devops, the whole content will disappears, and the include link won't work anymore. Please, notice that this action will break the pipeline of developers using your job.

??? info "Why do you delete completely a job?"

     In order to respect the job's owner privacy and choice, we decided to completely delete the content of the job and its access when the owner asks for it.

### Labels

??? info "What are the blue labels?"

    Blue labels give information about the origin of the jobs.

    In R2Devops, blue labels look like this:

    ![Blue label example](./images/label-official.png){width=20%}

    A job can be added in the hub by someone working for **R2Devops** or by someone from **the community**. If you [link a job from your repository](./link-a-job.md), your job will automatically get the label *community*. If you [contribute following R2Devops guidelines](./contribute.md), your job will get the label *Official*!

??? info "What means the label *Official*?"

    ![Blue label Official](./images/label-official.png){width=20%}

    The label Official means that the job respect [certains quality and security standard](../faq-use-a-job/).

??? info "What means the label *Community*?"

    ![Blue label community](./images/label-community.png){width=20%}

    The label Community means that the job was added by someone from the community. We don't know which quality standards this jobs follow, so we can't ensure it's safety.

??? info "What are the green labels?"

    Green labels give technical information about a job.

    In R2Devops, green labels look like this:

    ![Green label example](./images/label-green.png){width=20%}

??? info "How to add or remove a label?"

    You need to be the owner of a job to add a label. Once you are connected to your account, you can go on your *contributing dashboard*, click on the 3 dots on the right side of a job line and select *Modify my job*

    Once you are on this page, you can add or remove a label from the label input.

??? info "I can't find the label I want to use"

    If you can't find the label you want to use, you can [open a ticket](https://tally.so/r/w5Edvw) and ask our team to create a new label!
