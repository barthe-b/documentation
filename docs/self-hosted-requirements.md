# Requirements

!!! info "On-demand"
    The self-hosted alternative is only available on-demand.

    To begin the process, please refer to this [page](https://r2devops.io/pricing) for more information.

## 📄 Send your domain name

!!! info
    Your self-hosted instance of R2Devops must have a domain name. This is
    generally `r2devops.<your_main_domain>.com`.

There is some work to prepare on our side, and we need to know which domain you
will use. To do so, you have to send us which domain name you will use for the
application at [tech@r2devops.io](mailto:tech@r2devops.io).

In return, we will send you in a secured channel the 2 token and one tag
identifier you will need during setup:

- `REPOSITORY_TOKEN` : a token to access the repository
- `REGISTRY_TOKEN` : a token to authenticate to our private registry
- `FRONTEND_IMAGE_TAG` : a specific tag for your frontend image

!!! warning
    We don't save users tokens, so please keep them in a safe place.

## 💻 Prepare your server

The system is requiring a Linux server. It runs in 🐳 Docker containers using a
docker-compose configuration. Specifications:

- OS: Ubuntu or Debian
- Hardware
    - CPU x86_64/amd64 with at least 2 cores
    - 4 GB RAM
    - 30 GB of storage for R2Devops
- Network
    - Public IP
    - Ingress access on TCP ports 80, 443 and 22
    - Egress access on all ports
- Installed software
    - [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
    - [Docker](https://docs.docker.com/engine/install/)
    - Note: both compose plugin (`docker compose`) and `docker-compose` CLI are working. The first one is installed by default with `Docker`
