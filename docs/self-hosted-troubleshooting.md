# Troubleshooting

The installation is not working as expected ? You're at the right place !

Here are the common errors and how to solve them :

??? failure "CI/CD Catalog page load infinitely"
    This error occurs when the frontend application makes a request to the backend but never get a response.
    It is a misconfiguration of the frontend Docker image for your instance.

??? failure "Redirect URI Invalid error in GitLab"
    This error occurs when the Redirect URL set for your GitLab application doesn't correspond to the `API_URL`.
    Please, ensure you write the correct URL as described in the [section OIDC](/self-hosted-installation/#create-an-application)

??? failure "The website shows an error after a restore"
    The SSO uses several cookies to store the user session. If you restore a backup, it might happen that the cookies are not valid anymore. To fix this, you can clear the cookies and the cache for your domain name.
    Here are the steps, depending on your browser:

    - [Chrome](https://support.google.com/accounts/answer/32050?hl=en&co=GENIE.Platform%3DDesktop)
    - [Firefox](https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox)
    - [Safari](https://support.apple.com/guide/safari/manage-cookies-sfri11471/mac)

---

!!! question "Don't find what you're looking for ?"
    Reach our support using the `#support` channel on [Discord](https://discord.r2devops.io)

    You can also send an email to [tech@r2devops.io](mailto:tech@r2devops.io)
