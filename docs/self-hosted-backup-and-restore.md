# Backup and restore

## 📋 Introduction

Data required to fully backup and restore a R2Devops system are the following:

- Configuration file:  `.env`
- Databases:
    - PostgreSQL database of Jobs service
    - PostgreSQL database of Kratos service
- Files data:
    - Files stored in the Minio service
    - File storing data about certificate for Traefik service

All these data can be easily backup and restored using 2 scripts from the
installation git repository:

- `backup.sh`
- `restore.sh`

## 💽 Backup

To backup the system, go to your installation git repository and run the
following command:

```bash
./backup.sh
```

The script will create a `backups` directory and create a backup archive inside
it prefixed with the date (`backup_r2-$DATE`)

!!! note " Regular backup"
    You can use a cron job to perform regular backups.
    Here is a cron job that launch a backup every day at 2am:
    ```bash
    0 2 * * * /r2devops/backup.sh
    ```
    It can be added to your crontab with the command `crontab -e`.
    Check more information about cron jobs [here](https://help.ubuntu.com/community/CronHowto).

## 🛳️ Restore

To restore a backup from scratch on a new system, follow this process:

1. Be sure that your new system is compliant with
   [requirements](/self-hosted-requirements/#prepare-your-server)
1. Copy the backup file on your new server
1. Setup your environment
    ```bash
    export REPOSITORY_TOKEN="REPLACE_ME"
    export REGISTRY_TOKEN="REPLACE_ME"
    ```
1. Clone the installation repository
    ```bash
    git clone https://r2devops:${REPOSITORY_TOKEN}@gitlab.com/r2devops/self-hosted.git r2devops
    cd r2devops
    ```
1. If the IP address of your server changed from your previous installation,
   update your DNS records. See [section
   2](/self-hosted-installation/#domain-name) of domain configuration
1. Login to R2Devops registry
    ```bash
    echo $REGISTRY_TOKEN | docker login --username r2devops --password-stdin https://registry.gitlab.com/v2/r2devops
    ```
1. Launch the restore script
    ```bash
    ./restore.sh <path_to_your_backup_file>
    ```

!!! error "Any errors during the restore process ?"
    Did you encounter a problem during the restore process ? See the
    [troubleshooting](/self-hosted-troubleshooting) section.
