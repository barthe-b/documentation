---
title: How to build a CI/CD job
description: Discover the content of a job - the definition, metadata, documentation, changelogs and screenshots are simply described.
---

# Structure of official job repo

Our jobs are stored in the [R2Devops hub](https://gitlab.com/r2devops/hub)
repository inside the
[`jobs`](https://gitlab.com/r2devops/hub/-/tree/latest/jobs) folder, and
follow this standardized structure:

```
.
└── jobs
    └── <job_name>
        ├── <job_name>.yml        # Job definition
        ├── CHANGELOG.md          # Job changelog
        └── README.md             # Job documentation
```

*A template of job is available in the [R2Devops hub repository](https://gitlab.com/r2devops/hub/-/tree/latest/tools/job_template/job_name).*

## 🤖 Job definition

This file must have the same name as the job with the `yml` extension:
`<job_name>.yml`. It contains the GitLab job configuration in `yaml` format.

The jobs of the hub use the GitLab CI/CD configuration format. They must specify a Docker image to be run in a container.

!!! info
    If you are curious and want to know more about the job definition, you can go to:

    * [GitLab CI/CD pipeline configuration reference](https://docs.gitlab.com/ee/ci/yaml/){:target=blank}.
    * [R2Devops guidelines and best practices](/contribute/#guidelines) about job definition.

A job definition usually contains the following fields:

* **[`image`](https://docs.gitlab.com/ee/ci/yaml/#image){:target="_blank"}**: this is the docker image used to run the job.
* **`stage`** (mandatory): this is the default stage for the job. You can choose it in our [default stage list](/use-the-hub/#stages).
* **[`script`](https://docs.gitlab.com/ee/ci/yaml/#script){:target="_blank"}** (mandatory): this is the heart of the job. It contains a list of shell commands that run the job.
* **[`variables`](https://docs.gitlab.com/ee/ci/yaml/#variables){:target="_blank"}**: in this field, you will find all the variables used by the `script` of the job. This is where you customize its behavior.
* **[`artifacts`](https://docs.gitlab.com/ee/ci/yaml/#artifacts){:target="_blank"}**: it specifies the result of the job that should be exposed to the user through classic artifacts or Gitlab reports.

**Here is an example of job definition [`apidoc.yml`](https://r2devops.io/_/r2devops-bot/apidoc) 👇**

```yaml
apidoc:
  image:
    name: node:12.18.3-alpine3.12
    entrypoint: [""]
  stage: build
  variables:
    APIDOC_CONFIG_PATH: '.'
    APIDOC_OUTPUT_PATH: 'website_build/'
    APIDOC_TEMPLATE_PATH: '/usr/local/lib/node_modules/apidoc/template/'
    APIDOC_VERSION: '0.24.0'
  script:
    - npm install apidoc@$APIDOC_VERSION -g
    - apidoc -c "$APIDOC_CONFIG_PATH" -o "$APIDOC_OUTPUT_PATH" -t "$APIDOC_TEMPLATE_PATH"
  artifacts:
    when: always
    expose_as: "apiDoc build"
    paths:
      - "$APIDOC_OUTPUT_PATH"
```


## 📚 Job documentation

This file, named `README.md`, contains the documentation of a job  in `markdown` format.

!!! info
      The documentation explains what the job does, how to use it and to customize it. A clear documentation is important: no one wants to use a job when you can't understand what it is for!

=== "Example of README.md"

    ```md
    ## Objective

    Creates a versioned HTML documentation from API annotations in your source
    code using [apiDoc](https://www.apidocjs.com/).

    ## How to use it

    1. Prepare your project with API annotations in your source code following
       [apiDoc format](https://apidocjs.com/#examples) and your [apiDoc
       configuration file](https://apidocjs.com/#configuration).
    2. Choose a version in [version list](#changelog)
    3. Add the corresponding URL to your `.gitlab-ci.yml` file (see [Getting
       started](/use-the-hub/)). Example:
        ```yaml
        include:
        - remote: 'https://api.r2devops.io/job/r/r2devops-bot/apidoc'
        ```
    4. If you need to customize the job (stage, variables, ...) 👉 check the
       [jobs customization](/use-the-hub/#jobs-customization)
    5. Well done, your job is ready to work ! 😀

    ## Variables

    | Name | Description | Default |
    | ---- | ----------- | ------- |
    | `APIDOC_VERSION` | Version of apiDoc to use | `0.24.0` |
    | `APIDOC_CONFIG_PATH` | Path to config file or to directory containing config file (apidoc.json or apidoc.config.js) | `.` |
    | `APIDOC_OUTPUT_PATH` | Output directory path | `/documentation_build` |
    | `APIDOC_TEMPLATE_PATH` | Path to template folder | `/usr/lib/node_modules/apidoc/template/` |
    ```

=== "Result"

    ![Documentation result](images/documentation_result.png){: .documentation_result }


## 🏗 Job changelogs

This file, named `CHANGELOG.md`, contains the changelog of a job following the
[keep a changelog](https://keepachangelog.com/en/1.0.0/) structure and using
markdown.

!!! info
    * The jobs version should follow the [semantic versioning](https://semver.org/){:target=blank}
    format (`MAJOR.MINOR.PATCH`).
    * The first version recommended for a job is `0.1.0`.
    * For each version listed in this file, you should have a corresponding git tag. Both format are supported
        * Only the version as tag name. Ex: `1.1.4`
        * The job name and version, joined with a hyphen (`-`), as tag name. It is useful if you store several jobs in your repository. Ex: `docker_build-1.1.4`

**Here is an example of a `CHANGELOG.md` file for `docker_build` job 👇**

```markdown
# Changelog
All notable changes to this job will be documented in this file.

## [1.1.0] - 2021-06-21
* Allows context different from root with new variable `DOCKER_CONTEXT_PATH`

## [1.0.0] - 2021-05-07
* Breaking change in the configuration of custom registry, see documentation
* Add support to push in multiple registries
* Add support to authentication in multiple registries

## [0.4.0] - 2021-04-19
* Add new option `--use-new-run` to kaniko executor (enabled by default)
* Update kaniko image to `v1.5.1`

## [0.3.0] - 2020-11-25
* New variable `DOCKER_USE_CACHE` to be able to cache layers of build
* New variable `DOCKER_CACHE_TTL` to define time to live of cache
* New variable `DOCKER_VERBOSITY` to set the verbosity of the build
* New variable `DOCKER_OPTIONS` to be able to add additional options

## [0.2.0] - 2020-11-02
* Add variable `DOCKERFILE_PATH` which permits specifying custom path to
  Dockerfile

## [0.1.0] - 2020-10-21
* Initial version
```
