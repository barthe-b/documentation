# Create or import my private CI/CD catalog

In order to **create your private CI/CD catalog**, you need to import your jobs from GitLab.
For that, connect to [r2devops.io](https://r2devops.io/) and click on the **Imports** button in the sidebar.

## **Now you have two choices:**
### 👉 You need to import just one job
Fill in the form with information about the repository containing your jobs.

!!! info "You don't know where to find these information?"
    Check the documentation about [importing your resources](/get-started-link-job/#how-to-link-your-job).

Once it's done, you need to define the visibility of your jobs on private. You job will only be accessible from you or your organization members.


###  👉 You need to import all your jobs from your GitLab directory

Same principle as importing a job, but you don't have to give the name of the job you want to import! All your jobs from your GitLab repository will be automatically imported.


## **Congrats, your private CI/CD catalog is created!**
Easy, isn't it?
