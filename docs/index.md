---
title: R2Devops documentation
description: Save time in your development projects - Create powerful CI/CD pipelines at rocket speed using R2Devops !
---

# Welcome

R2Devops is the place to get and manage your CI/CD pipeline, powered by opensource tools.

- Manage organizations and private resources. 
- Generate in one click a pipeline that automates development supply-chain painful tasks.
- Focus on what you love: coding!

!!! info "Getting started 🚀"
    ## ⚡ Use R2Devops
    ### What do you want to do? 🤔  
    
     <a alt="Open a ticket" href="./create-catalog">
    <button class="md-button border-radius-10 md-button" >
       🔒 Create a private CI/CD catalog for my organization
    </button>
    </a>
    <a alt="Open a ticket" href="./get-started-use-the-hub/">
    <button class="md-button border-radius-10 md-button" >
         📄 Use a CI/CD template  
    </button>
    </a>  
      
    <a alt="Open a ticket" href="./get-started-contribute-job/">
    <button class="md-button border-radius-10 md-button" >
        👨‍💻 Contribute to R2Devops CI/CD catalog
    </button>
    </a>
    <a alt="Open a ticket" href="./get-started-link-job/">
    <button class="md-button border-radius-10 md-button" >
        ➕ Add a template to R2Devops catalog
    </button>
    </a>  
      
    <a alt="Open a ticket" href="./get-started-pipeline/">
    <button class="md-button border-radius-10 md-button" >
        🚀 Generate a CI/CD pipeline in 1-click
    </button>
    </a>


!!! heart "Community"
    We love talking with our contributors and users! Join our
    [Discord community :fontawesome-brands-discord:](https://discord.r2devops.io/?utm_medium=website&utm_source=r2devopsdocumentation?utm_campaign=addajob)



## Can't find the information you need ?

You can join the support team on Discord or open a ticket 👇

<a alt="Join R2Devops.io Discord" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=homepage" target="_blank">
    <button class="md-button border-radius-10 md-button" >
        💬 On Discord
    </button>
</a>

<a alt="Open a ticket" href="https://tally.so/r/w5Edvw" target="_blank">
    <button class="md-button border-radius-10 md-button" >
        🎟 Open a ticket
    </button>
</a>
