---
title: Manage your pipelines at rocket speed
description: Learn how to manage pipelines of your projects in a few clicks using R2Devops
---

# Generate and maintain your pipelines

With R2Devops, you can automatically generate a pipeline for your project using **Pipeline Generator** 👇

## With an account

If you already have an account, all your projects will be listed in `My Projects` dashboard. 

![Pipeline Generator in the project dashboard](./images/pipelinegenerator.gif)

!!! info "You can generate a pipeline for **public and private projects**."
     Once you have generated a pipeline for a project, R2 will check every day and warn you if updates are available for it. It's the **Pipeline Updates** feature!

     You'll be notified in your dashboard: the button `Generate a pipeline` will turn orange for the project you can improve.     


## Without an account

If you don't have an account, you can access Pipeline Generator from the [homepage](https://r2devops.io), or from the [Pipeline generator](https://pipeline.r2devops.io) page. 

![Pipeline Generator in R2Devops' homepage](./images/pipelinegenerator_homepage.gif)

!!! info "Without account, you can only generate a pipeline for **public projects**."

     Don't forget to give feedback on the pipeline created, by answering the Yes/ No question. It will help us improve our process.

     The feature **Pipeline Updates** is not available on those pages. You need to create an account in R2Devops to use it.

## You have an account but no projects

If you don't have any projects to use the pipeline generator on, you can test it on some open source projects available on the generator: 

* **Python**: Flask based REST API permitting to play TicTacToe 👉 [tictactoe/grid-api](https://gitlab.com/tictac-toe/grid-api)
* **Node**: Vue.js project providing a client to the TicTacToe API 👉 [tictactoe/grid-frontend](https://gitlab.com/tictac-toe/grid-frontend)
* **Golang**: DualStack Service LoadBalancer controller, that uses standard Linux networking and routing protocols 👉 [purelb/purelb](https://gitlab.com/purelb/purelb)
* **Java** : An awesome free and open source RSS Feed reader 👉 [spacecowboy/Feeder](https://gitlab.com/spacecowboy/Feeder)
