---
title: Jobs versioning
description: Have you ever wonder how versioning works? We tell you all! You will understand what latest means and how to include properly a job in your CI/CD pipeline.
---

# Versioning

Each jobs is versioned following a `CHANGELOG.md` file, where all the versions and changes are centralized.

Example of the `CHANGELOG.md` of the [maven_build](https://r2devops.io/_/r2devops-bot/maven_build) job : 

```md
# Changelog
All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-03-24
* Upgrade image to 3.8.4-jdk-11

## [0.1.0] - 2021-03-25
* Initial version
```

!!! note
    Version follows the [Semantic Versioning](https://semver.org/){:target="_blank"}.

You can also use the latest version, using `latest` instead of a tag. Applying
this, you will retrieve the latest version of jobs at each run. Note that if
you don't set any tag, `latest` is used by default.

!!! warning
    By using the latest version, you are taking a risk. Each time modification will be pushed, the latest version will change, and your pipeline might break.
    To avoid it, we recommend you to set only fixed version for the jobs you are using in your pipeline.

Each jobs can be used independently with a different version.

Example in `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://api.r2devops.io/job/r/r2devops-bot/docker_build'
  - remote: 'https://api.r2devops.io/job/r/r2devops-bot/mkdocs/1.5.1.yaml'
  - remote: 'https://api.r2devops.io/job/r/r2devops-bot/apidoc/0.3.1.yaml'
```

Available tags and release note for each job are available in the [jobs section](https://r2devops.io/jobs).

--8<-- "includes/abbreviations.md"
